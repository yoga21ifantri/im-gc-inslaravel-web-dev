<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>

<body>
    <h1>Berlatih String PHP</h1>

    <?php
    /*Soal NO 1 Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut!*/
    echo "<h3>Soal No 1</h3>";
    
    $first_sentence = "Hello PHP!";
    $second_sentence = "I'm ready for the challenges";
    
    // menghitung panjang string dan jumlah kata pada kalimat pertama
    $first_sentence_length = strlen($first_sentence);
    $first_sentence_words = str_word_count($first_sentence); 
    // menampilkan hasil dari kalimat pertama
    echo "Sentence: " . $first_sentence . "<br> Panjang string: " . $first_sentence_length . ", Jumlah kata: " . $first_sentence_words . "<br>";
    
    // menghitung panjang string dan jumlah kata pada kalimat kedua
    $second_sentence_length = strlen($second_sentence);
    $second_sentence_words = str_word_count($second_sentence); 
    // menampilkan hasil dari kalimat kedua
    echo "<br>Sentence: " . $second_sentence . "<br> Panjang string: " . $second_sentence_length . ", Jumlah kata: " . $second_sentence_words;
    
    
    /*Soal No 2 Mengambil kata pada string dan karakter-karakter yang ada*/

    echo "<h3> Soal No 2</h3>";
    
    $string2 = "I love PHP";
    echo "Kata pertama: " . substr($string2, 0, 1) . "<br>";
    echo "Kata kedua: " . substr($string2, 2, 4) . "<br>";
    echo "Kata ketiga: " . substr($string2, 7, 3) . "<br>";
    

    /* SOAL NO 3 Mengubah karakter atau kata yang ada di dalam sebuah string. */

    echo "<h3> Soal No 3 </h3>";

    $string3 = "PHP is old but sexy!";
    // mengganti kata "sexy" dengan kata "awesome"
    $new_string3 = str_replace("sexy", "awesome", $string3);
    echo "$new_string3";
    ?> 

<a href="array.php"><h3><b>NEXT</b></h3></a>
</body>



</html>