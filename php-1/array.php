<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array PHP</title>
</head>

<body>
    <h1>Berlatih Array PHP</h1>
    <?php

    /*SOAL NO 1 Kelompokkan nama-nama di bawah ini ke dalam Array. Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" Adults: "Hopper", "Nancy", "Joyce", "Jonathan", "Murray" */
    echo "<h3>Soal No 1</h3>";
    
    $kids = array("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven");
    $adults = array("Hopper", "Nancy", "Joyce", "Jonathan", "Murray");
    echo "kids = Mike, Dustin, Will, Lucas, Max, Eleven";
    echo "<br>adults = Hopper, Nancy, Joyce, Jonathan, Murray";
    
    
    /*Soal No 2 Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array*/
    echo "<h3> Soal No 2</h3>";
    
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: " . count($kids) . "<br>";
    echo "<ol>";
    foreach ($kids as $kid) {
        echo "<li>$kid</li>";
    }
    echo "</ol>";
    echo "Total Adults: " . count($adults) . "<br>";
    echo "<ol>";
    foreach ($adults as $adult) {
        echo "<li>$adult</li>";
    }
    echo "</ol>";
   
    
    /* SOAL NO 3 Mengubah karakter atau kata yang ada di dalam sebuah string. */
    echo "<h3> Soal No 3 </h3>";

    $data = array(
        array(
            "Name" => "Will Byers",
            "Age" => 12,
            "Aliases" => "Will the Wise",
            "Status" => "Alive"
        ),
        array(
            "Name" => "Mike Wheeler",
            "Age" => 12,
            "Aliases" => "Dungeon Master",
            "Status" => "Alive"
        ),
        array(
            "Name" => "Jim Hopper",
            "Age" => 43,
            "Aliases" => "Chief Hopper",
            "Status" => "Deceased"
        ),
        array(
            "Name" => "Eleven",
            "Age" => 12,
            "Aliases" => "El",
            "Status" => "Alive"
        )
    );
    
    $output = array();
    
    foreach ($data as $item) {
        $output[] = array(
            "Name" => $item["Name"],
            "Age" => $item["Age"],
            "Aliases" => $item["Aliases"],
            "Status" => $item["Status"]
        );
    }
    
    echo "<pre>";
    print_r($output);
    echo "</pre>";
    ?> 
</body>
</html>